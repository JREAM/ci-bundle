# Code Igniter File Model
---

This is a sub-model to place within models for Code Igniter 2.X.
This is in Alpha at the moment and not finished.

---
# Contents

1. Description
2. Requirements
3. Setup SQL
4. Setup PHP

---
# Description
---
There are three tables:

 - FILE TABLE: `file`
 - GROUP ASSOCIATION: `file_group`
 - UNIQUE ID TABLE: `file_group_id`

You then associate a group_id within any other table:

 - PROJECT TABLE: Contains a foreign key to `file_group`

# Requirements

Enable PHP Fileinfo extension for verifying the MIME Type

    extension=php_fileinfo.so

Define two constants `UPLOAD_PATH` and `AWS_BUCKET_NAME`

    define('UPLOAD_PATH', realpath('upload') . '/');
    define('AWS_BUCKET_NAME', ''); // Leave empty if not using

If you are using S3, I've included the third parties folder to include the class from:

    http://undesigned.org.za/2007/10/22/amazon-s3-php-class


# Setup SQL
Provided is a MySQL Schema to import.

# Setup PHP
---

1. Place File_model in your **/application/models** folder
2. Create a new model, such as `project_model.php`.
3. Instantiate the File model within the project_model:

    public function __construct()
    {
        // Load a fresh instance of the Meta Model
        if (!class_exists('File_model')) {
            require APPPATH . 'models/file_model.php';
        }
        $this->file = new File_model();
    }

4. Access the model with: `$this->project_model->file->upload()`

----
(C) 2013 Jesse Boyer <http://jream.com>