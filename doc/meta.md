# Code Igniter Meta Model
---

This is a re-usable SQL meta model for Code Igniter 2.X. This ended up saving me a lot of time.
So here it is, simple and open source in all it's glory.

---
# Contents

1. Description
2. Setup SQL
3. Setup PHP
4. Usage
    - Set (Insert/Update)
    - Get (Single, Bundle)
    - Delete (Single, Group, All)

---
# Description
---
There are two tables, a **parent** table and a **meta** table. The table are paired by a **reference_key** shared on both tables. An example would be:

 - PARENT: `user`
 - META: `user_meta`
 - REFERENCE_KEY: `user_id`

or

 - PARENT: `file`
 - META: `file_meta`
 - REFERENCE_KEY: `file_id`


# Setup SQL
Provided is a MySQL Schema that you can import, or customize to your needs. There are two key factors that matter:

 - META: Must be the parent table name with the suffix of '_meta'
 - REFERENCE_KEY: Must be the parent table name with the suffix of '_id'. This can be changed if you like, but it must be changed in both tables.


# Setup PHP
---

1. Place Meta_model in your **/application/models** folder
2. Create a new model or controller. Provided in the files is a controller example, below will be a model example. *(I'll call mine User_model)* and extend the **Meta_model**
Q: Why would I want to subclass a model? Because you can treat your models as one by using this syntax: `$this->user->meta->get()` -- This is optional, you can use a controller if you prefer.

Here is example code for **User_model.php**:

    <?php

    class User_model extends CI_Model {

        public function __construct()
        {
            parent::construct();
            $this->load->model('meta_model', 'meta');
            $this->meta->init('user');
        }
    }

# Usage
---

Meta items are in Groups, then given Keys. For example, if you are organizing Users data, you group  something like this:

 - Hobby: Fishing, Skiing
 - Status: Single, Graduated

# Set(#set)
---
Here is how you can execute settings meta data. If a key exists it will update the record, not insert a new one because that's impossible with our Schemas unique key/pair settings.

    <?php
    // Load the model we want to use
    // Notice that I have aliased the user_model as user
    $this->load->model('user_model', 'user');

    // A random id for the "reference_id"
    $id = 25;

    // set(group, key, value, reference_id);
    $this->user->meta->set('hobby', 'fishing', 1, $id);
    $this->user->meta->set('hobby', 'skiing', 1, $id);

    $this->user->meta->set('status', 'married', 0, $id);
    $this->user->meta->set('status', 'graduated', 0, $id);


# Get
---

Getting a group of data comes out nice, all associative arrays:

    <?php
    $this->user->meta->get('hobby');

Or grab a single items value

    <?php
    $this->user->meta->get('hobby', 'fishing');

## Get a Bundle
A bundle is the parent table and the meta data included.

### Return Many Bundles

    <?php
    // Return all Parent and Meta in an enumerated array
    $this->user->meta->get_bundle();

    // Optionally you can pass in custom parameters for the parent table
    $this->user->meta->get_bundle(array('email' => 'ted@ted.com'));


**Example Output:**

    Array
    (
        [0] => Array
        (
            [user_id] => 25,
            [email] => nobody@nobody.com,
            [profile] => Array
                (
                    [first_name] => Ted
                    [last_name] => Smith
                    [address] => 500 Heaven
                    [city] => Orlando
                    [state] => FL
                    [zip] => 32810
                )
        )
        [1] => Array
        (
            [user_id] => 26,
            [email] => nobody@nowhere.com,
            [profile] => Array
                (
                    [first_name] => Jack
                    [last_name] => Smith
                    [address] => 500 Greenwood
                    [city] => Wyoming
                    [state] => MI
                    [zip] => 49509
                )
        )
    )

### Return one Bundle

    <?php
    // Return one Parent and Meta in an associative array
    $this->user->meta->get_bundle(25);

**Example Output:**
    Array
    (
        [user_id] => 25,
        [email] => nobody@nobody.com,
        [profile] => Array
            (
                [first_name] => Ted
                [last_name] => Smith
                [address] => 500 Heaven
                [city] => Orlando
                [state] => FL
                [zip] => 32810
            )

    )

# Delete
---

Deleting is also simple. You can delete based on a group/key and optional reference_id.

### Delete one Meta record

    <?php
    $user_id = 25;
    $this->user->meta->delete('profile', 'address', $user_id);

### Delete all Meta keys of a given group

    <?php
    // Without providing a reference_id you can delete all keys in a group
    $this->user->meta->delete('profile', 'address');

### Delete a Meta Group for one user
You can also delete an entire group based on a group name and an optional reference ID.

    <?php
    $user_id = 25;
    $this->user->meta->delete_group('profile', $user_id);

### Delete a Meta Group for one user
This will delete all profile groups!

    <?php
    $this->user->meta->delete_group('profile');


----
(C) 2013 Jesse Boyer <http://jream.com>