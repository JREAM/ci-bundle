<?php
/**
 * @author      Jesse Boyer <contact@jream.com>
 * @copyright   Copyright (C), 2013 Jesse Boyer
 * @license     GNU General Public License 3 (http://www.gnu.org/licenses/)
 *              Refer to the LICENSE file distributed within the package.
 *
 * @version     0.1a
 * @link        http://jream.com
 *
 * This is a file sub-model which should be used as a subclass inside of another model.
 *
 * @requirements
 *     extension=php_fileinfo.so
 *
 *     define('UPLOAD_PATH', realpath('upload') . '/');
 *     define('AWS_BUCKET_NAME', ''):
 *
 *     If using S3, make sure this package gets loaded:
 *     http://undesigned.org.za/2007/10/22/amazon-s3-php-class
 *
 * DB Schema:
 * ---
 *  There are 3 tables needed for this Many 2 Many relationship:
 *      file
 *      file_group
 *      file_group_id
 *
 *  Any model that uses this sub-model would need a foreign key to file_group_id.
 *  For example, a project table would need
 *      project.file_group_id
 *
 * @example
 *
 * Inside "project_model.php" we want to create a clean re-usable model.
 * ---
 * public function __construct()
 * {
 *     // Load a fresh instance of the Meta Model
 *     if (!class_exists('File_model')) {
 *         require APPPATH . 'models/file_model.php';
 *     }
 *     $this->file = new File_model();
 * }
 *
 * Usage
 * ---
 * $this->project_model->file->upload_local();
 * $this->project_model->file->upload_s3();
 * $this->project_model->file->insert();
 * $this->project_model->file->delete();
 * etc..
 */
class File_model extends CI_Model {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    public function get_group($group_id)
    {
        if (!is_numeric($group_id)) {
            return false;
        }

        $this->db->join('file', 'file.file_id = file_group.file_id');
        $q = $this->db->get('file_group');
        $result = $q->result_array();

        return $result;
    }

    // ------------------------------------------------------------------------

    /**
     * Inserts Database Record for M2M relationship
     *
     * @param  array  $insert_data  Associative Array of title, path, size
     * @param  boolean|integer $group_id (Optional) Attach to a existing group of files,
     *                                   otherwise it creates a new one if false
     *
     * @return boolean
     */
    public function insert($insert_data, $group_id = false)
    {
        // Begin a Transaction
        $this->db->trans_begin();

        // Insert the File Record
        $this->db->insert('file', $insert_data);

        // Gather the File ID
        $file_id = $this->db->insert_id();

        if ($group_id == false)
        {
            // Create a new Group ID
            // The UUID is so I can get a primary key, I dont know how to insert
            // nothing into the table and get a primary key.
            $this->db->insert('file_group_id', array(
                'uuid' => uniqid()
            ));

            // Gather the Group ID
            $group_id = $this->db->insert_id();
        }

        // Pair the Group ID and File ID together
        $this->db->insert('file_group', array(
            'group_id' => $group_id,
            'file_id' => $file_id
        ));

        // Rollback upon failure
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        // Commit upon success
        $this->db->trans_commit();
        return true;
    }

    // ------------------------------------------------------------------------

    /**
     * Deletes from the file and file_group table
     *
     * @param  integer $file_id
     * @return boolean
     */
    public function delete($file_id)
    {
        $this->db->delete('file', array('file_id' => $file_id));
        $this->db->delete('file_group', array('file_id' => $file_id));
        return true;
    }
    // ------------------------------------------------------------------------

    /**
    * Uploads to the local upload directory
    *
    * @param resource $file Pass in the $_FILE['name']
    * @param array $params Associative array:
    *                          name=> filename (no extension),
    *                          type => all|image|doc|audio|video
    * @param integer $user_id (Optional) If you are not using CI session 'user_id'
    *                          use this because a user_id is passed into the filename.
    *
    * @return  array Associative array with the result and message
    */
    public function upload_local($file, $params, $user_id = false)
    {
        // Assume the default session class
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        if (!is_array($params)) {
            return array(
                 'result' => 0,
                 'message' => 'Params must be an array'
            );
        }

        // Set a default name if none is passed
        if (!isset($params['name'])) {
            $params['name'] = 'upload_' . uniqid() . '_';
        }

        // Set a default type if none is passed
        if (!isset($params['type'])) {
            $params['type'] = 'all';
        }

        // Obviously we need a file
        if (empty($file)) {
            return array(
                 'result' => 0,
                 'message' => 'No file provided.'
            );
        }

        // Make sure there are no file errors
        if ($file['error'] > 0) {
            return array(
                 'result' => 0,
                 'message' => 'Sorry, there was a file error.'
            );
        }

        // Clean up this extension
        $ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

        // Get the tmp_name from the $_FILES object
        $tmp_name = $file['tmp_name'];

        // Create a custom avatar name based on the user_id
        $file_name = $params['name'] . $user_id . '.' . $ext;

        // Move the file to our desired upload destination (set in config/app_config.php)
        $result = move_uploaded_file($tmp_name, UPLOAD_PATH . $file_name);

        // Make sure it's moved
        if ($result == false)
        {
            log_message('error', __CLASS__ . '::' . __FUNCTION__ . ': Error moving file.');
            return array(
                 'result' => 0,
                 'message' => 'Error moving file.'
            );
        }

        // Make sure the type is valid, it has to exist on the server
        // to validate the MIME type.
        switch ($params['type'])
        {
            case 'img':
            case 'image':
                $type_success = $this->_is_image(UPLOAD_PATH . $file_name);
                break;
            case 'doc':
            case 'document':
                $type_success = $this->_is_doc(UPLOAD_PATH . $file_name);
                break;
            case 'video':
                $type_success = $this->_is_video(UPLOAD_PATH . $file_name);
                break;
            case 'audio':
                $type_success = $this->_is_audio(UPLOAD_PATH . $file_name);
                break;
            default:
                break;
        }

        // If the filetype is not right, stop here.
        if ($type_success == 0) {
            return array(
                 'result' => 0,
                 'message' => 'Invalid File Type.'
            );
        }

        // Otherwise, we have done well
        return array(
             'result' => 1,
             'file_name' => $file_name,
             'message' => 'Success.'
        );
    }

    // ------------------------------------------------------------------------

    /**
    * Delete local file
    *
    * @param string $file Delete the filename
    *
    * @return  boolean
    */
    public function delete_local($file)
    {
        if (file_exists($file)) {
            unlink($file);
            return true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
    * Uploads to S3 bucket
    * Removes local copy by default
    *
    * @param string $file Local file to upload to S3
    * @param string $s3_folder Place in a subfolder of the bucket
    * @param boolean $rm_local (Optional)
    */
    public function upload_s3($file, $s3_folder = null, $rm_local = true)
    {
        $this->_is_s3_setup();

        S3::putObject(
              S3::inputFile(UPLOAD_PATH . $file, false),
              AWS_BUCKET_NAME . '/' . trim($s3_folder, '/'),
              $file,
              S3::ACL_PUBLIC_READ
        );

        // Remove local files once they are in S3
        if ($rm_local) {
            $this->delete(UPLOAD_PATH . $file);
        }

        return array(
            'result' => 1,
            'message' => 'Success.'
        );
    }

    // ------------------------------------------------------------------------

    /**
    * Deletes from S3 bucket
    *
    * @param string $file Local file to upload to S3
    *
    * @return array
    */
    public function delete_s3($file)
    {
        $this->_is_s3_setup();

        S3::deleteObject(AWS_BUCKET_NAME, $file);
        return array(
            'result' => 1,
            'message' => 'Success.'
        );
    }

    // ------------------------------------------------------------------------

    /**
     * Make sure S3 is setup
     */
    private function _is_s3_setup()
    {
        if (!defined('AWS_BUCKET_NAME')) {
            die('Please define your AWS_BUCKET_NAME in your constants file');
        }

        if (!class_exists('S3')) {
            die('Please include you S3 class in your third-party folder');
        }
    }
    // ------------------------------------------------------------------------

    /**
    * Creates a thumbnail
    *
    * @param string $file Local file to upload to S3
    * @param integer $height
    * @param integer $width
    * @param integer $user_id (Optional) If you are not using CI session 'user_id'
    *                          use this because a user_id is passed into the filename.
    *
    * @return array Associative array with result and message
    */
    public function make_thumbnail($source, $width = 150, $height = 150, $user_id = false)
    {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }

        $config = array(
            'image_library' => 'gd2',
            'source_image' => UPLOAD_PATH . $source,
            'new_image' => UPLOAD_PATH . $source,
            'create_thumb' => true, // Preserves original image
            'thumb_marker' => '_th',
            'maintain_ratio' => true,
            'width' => $width,
            'height' => $height
        );
        $this->load->library('image_lib', $config);

        if (!$this->image_lib->resize()) {
            return array(
                 'result' => 0,
                 'message' => 'Problem resizing image.'
            );
        }

        // Get the real filename
        $ext = strtolower(pathinfo(UPLOAD_PATH . $source, PATHINFO_EXTENSION));
        $real_file = basename(UPLOAD_PATH . $source);
        $real_file = substr($real_file, 0, strrpos($real_file, '.'));
        $real_file = $real_file . $config['thumb_marker'] . '.' . $ext;

        // Success
        return array(
            'result' => 1,
            'file_name' => $real_file,
            'message' => 'Success.'
        );
    }

    // ------------------------------------------------------------------------

    /**
    * Is this file an image?
    *
    * @param string $file Path to the file
    *
    * @return boolean
    */
    private function _is_image($file)
    {
        $success = true;

        $mime = $this->_get_mime($file);

        // Validate the extension
        if (!in_array($mime['ext'], array('jpg', 'jpeg', 'png', 'gif'))) {
            $success = false;
        }

        // validate the accepted MIME type
        $accepted_mimes = array(
            'image/gif',
            'image/jpeg',
            'image/png'
        );

        if (!in_array($mime['type'], $accepted_mimes)) {
            $success = false;
        }

        if ($success == false) {
            $this->delete_local($file);
            return false;
        }

        return true;
    }

    // ------------------------------------------------------------------------

    private function _is_doc()
    {
    }

    // ------------------------------------------------------------------------

    private function _is_video()
    {
    }

    // ------------------------------------------------------------------------

    private function _is_audio()
    {
    }

    // ------------------------------------------------------------------------

    private function _is_binary()
    {
    }

    // ------------------------------------------------------------------------

    /**
    * Gets the Extension and Mime Type
    *
    * @param string $file Local file
    *
    * @return array
    */
    private function _get_mime($file)
    {
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $file);

        return array(
            'ext' => $ext,
            'type' => $mime
        );
    }

    // ------------------------------------------------------------------------

}