<?php
/**
 * @author      Jesse Boyer <contact@jream.com>
 * @copyright   Copyright (C), 2013 Jesse Boyer
 * @license     GNU General Public License 3 (http://www.gnu.org/licenses/)
 *              Refer to the LICENSE file distributed within the package.
 *
 * @version     0.5
 * @link        http://jream.com
 */
class Message_model extends CRUD_model {

    // ------------------------------------------------------------------------

    protected $_table    = 'message';
    protected $_primary_key = 'message_id';

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    /**
    * @param int $user_id
    */
    public function get_all($user_id)
    {
        $this->db->where('message_user.parent_id', 0);
        $all = $this->_get_message($user_id);

        foreach($all as $_key => $_value) {
            // Append all unread values
            $all[$_key]['unread'] = $this->_has_read($_value['message_id']);
        }

        return $all;
    }

    // ------------------------------------------------------------------------

    /**
     * @param int $user_id
     * @param int $id
     */
    public function get_thread($user_id, $id)
    {
        $main = $this->_get_message($user_id, $id);

        // Due to the where statements as sender_id or receiver_id
        // This makes sure the message belongs to the person!
        if (empty($main)) {
            return false;
        }

        $this->db->where('message_user.parent_id', $id);
        $main[0]['threads'] = $this->_get_message($user_id);
        return $main;
    }

    // ------------------------------------------------------------------------

    /**
     * @param int $user_id
     * @param int $message_id
     * @param int $parent_id
     */
    private function _get_message($user_id, $message_id = false)
    {

        if ($message_id) {
            $this->db->where('message.message_id', $message_id);
        }

        $this->db->select('message.*,
                          receiver.alias receiver_alias,
                          sender.alias sender_alias,
                          message_user.receiver_id,
                          message_user.sender_id,
                          message_user.read
        ');
        $this->db->join('message_user', 'message.message_id = message_user.message_id');
        $this->db->join('user AS receiver', 'message_user.receiver_id = receiver.user_id');
        $this->db->join('user AS sender', 'message_user.sender_id = sender.user_id');
        // Only grab parent categories

        $this->db->having('sender_id', $user_id);
        $this->db->or_having('receiver_id', $user_id);

        $this->db->order_by('date_added ASC');
        $q = $this->db->get('message');

        return $q->result_array();
    }

    // ------------------------------------------------------------------------

    /**
    * Overrides the insert method to go into two tables
    */
    public function insert($params)
    {
        $required = array(
            'title',
            'content',
            'receiver_id',
            'parent_id',
            'date_added',
        );

        //$diff = array_diff($params, array_keys($required));
        //print_r($diff);

        $this->db->insert('message', array(
            'title' => $params['title'],
            'content' => $params['content'],
            'date_added' => DATETIME
        ));
        $id = $this->db->insert_id();

        $this->db->insert('message_user', array(
            'message_id' => $id,
            'sender_id' => $params['sender_id'],
            'receiver_id' => $params['receiver_id'],
            'parent_id' => $params['parent_id']
        ));

        return true;
    }

    // ------------------------------------------------------------------------

    /**
     * Return a flag for the receiver if a message has not yet been read
     * This is called separately because a new message could be added.
     *
     * @param  integer $thread_id
     * @return integer
     */
    private function _has_read($parent_id)
    {
        $user_id = $this->session->userdata('user_id');

        // Select the logged in user as the receiver of unread messages.
        // If the logged in user sent them, he obviously already read it.
        $this->db->select("count(`read`) unread");
        $q = $this->db->get_where('message_user', array(
            'parent_id' => $parent_id,
            'receiver_id' => $user_id,
            // Only select unread messages!
            'read' => 0
        ));

        $result = $q->result_array();

        if (!empty($result)) {
            return $result[0]['unread'];
        }

        return 0;
    }
    // ------------------------------------------------------------------------

}