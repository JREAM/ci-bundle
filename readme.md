# Code Igniter Bundle
---

A bundle of CodeIgniter Items for use in any CI project. The third party folder
is only needed for the **file_model** if you choose to use Amazon S3.

# Bundles

###CRUD Model
An easy to use Insert, Update, Get, Delete model to extend.

### Meta Model
Sub-model to include within a parent model. Handles arbitrary data for any model.

### File Model
Sub-model to include within a parent model. Handles file uploads, MIME validation, M2M Relationships.

### Message Model
Handles user messaging and message threads.

----
(C) 2013 Jesse Boyer <http://jream.com>