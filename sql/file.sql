-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.0.0.4421
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table bi-project.file
CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table bi-project.file: ~8 rows (approximately)
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` (`file_id`, `path`, `title`, `size`, `date_added`) VALUES
	(2, 'ahh', 'ahh', '44', '0000-00-00 00:00:00'),
	(6, 'file.jpg', 'test', '123', '2013-08-17 01:14:04'),
	(7, 'file.jpg', 'test', '123', '2013-08-17 01:14:10'),
	(8, 'file.jpg', 'test', '123', '2013-08-17 01:26:27'),
	(14, 'file.jpg', 'test', '123', '2013-08-17 01:43:37'),
	(17, 'paypal.JPG', 'test', '65552', '2013-08-17 01:55:30'),
	(18, 'paypal.JPG', 'test', '65552', '2013-08-18 00:12:01'),
	(19, 'download.jpg', 'test', '6985', '2013-08-18 00:12:26');
/*!40000 ALTER TABLE `file` ENABLE KEYS */;


-- Dumping structure for table bi-project.file_group
CREATE TABLE IF NOT EXISTS `file_group` (
  `group_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `group_id_file_id` (`group_id`,`file_id`),
  CONSTRAINT `FK_file_group_file_group_id` FOREIGN KEY (`group_id`) REFERENCES `file_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bi-project.file_group: ~8 rows (approximately)
/*!40000 ALTER TABLE `file_group` DISABLE KEYS */;
INSERT INTO `file_group` (`group_id`, `file_id`) VALUES
	(1, 2),
	(1, 14),
	(1, 17),
	(1, 18),
	(1, 19),
	(3, 6),
	(4, 7),
	(5, 8);
/*!40000 ALTER TABLE `file_group` ENABLE KEYS */;


-- Dumping structure for table bi-project.file_group_id
CREATE TABLE IF NOT EXISTS `file_group_id` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table bi-project.file_group_id: ~5 rows (approximately)
/*!40000 ALTER TABLE `file_group_id` DISABLE KEYS */;
INSERT INTO `file_group_id` (`group_id`, `uuid`) VALUES
	(1, NULL),
	(2, NULL),
	(3, '520ece5c266e4'),
	(4, '520ece6418a82'),
	(5, '520ed143cc97e');
/*!40000 ALTER TABLE `file_group_id` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
