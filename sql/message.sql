-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.0.0.4421
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table bi-project.message
CREATE TABLE IF NOT EXISTS `message` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table bi-project.message: ~12 rows (approximately)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`message_id`, `title`, `content`, `date_added`) VALUES
	(6, 'dasdf', 'hey', '2013-07-29 07:07:29'),
	(7, 'heya', 'hey\r\n', '2013-07-29 22:33:10'),
	(8, 'heya', 'hey\r\n', '2013-07-28 22:33:12'),
	(9, 'heyg', 'safsdf', '2013-07-30 02:32:22'),
	(10, '0', 'ge\r\n', '2013-07-30 02:35:10'),
	(11, '0', 'me was here', '2013-08-13 01:38:08'),
	(12, '0', 'more reply', '2013-08-13 01:38:15'),
	(13, '0', 'klj', '2013-08-13 02:25:37'),
	(14, '0', 'harr', '2013-08-13 02:26:18'),
	(15, '0', 'harr', '2013-08-13 02:26:20'),
	(16, 'hello', 'hey', '2013-08-18 20:21:02'),
	(17, '0', 'gw', '2013-08-18 20:21:13');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


-- Dumping structure for table bi-project.message_user
CREATE TABLE IF NOT EXISTS `message_user` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL,
  `receiver_id` int(10) unsigned NOT NULL,
  `read` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table bi-project.message_user: ~12 rows (approximately)
/*!40000 ALTER TABLE `message_user` DISABLE KEYS */;
INSERT INTO `message_user` (`message_id`, `parent_id`, `sender_id`, `receiver_id`, `read`) VALUES
	(6, 0, 2, 1, 0),
	(7, 0, 2, 1, 1),
	(8, 6, 1, 2, 0),
	(9, 0, 2, 1, 0),
	(10, 9, 2, 1, 0),
	(11, 7, 2, 1, 1),
	(12, 7, 2, 1, 0),
	(13, 7, 2, 1, 0),
	(14, 7, 2, 1, 0),
	(15, 7, 1, 2, 0),
	(16, 0, 2, 2, 0),
	(17, 16, 2, 2, 0);
/*!40000 ALTER TABLE `message_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
