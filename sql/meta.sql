/**
This is an example data schema.
You can do any table pair you like, just change the 'user_id' and 'table' names!
*/


/**
Example User META TABLE
*/
CREATE TABLE IF NOT EXISTS `user_meta` (
  `data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `group` varchar(50) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`data_id`),
  UNIQUE KEY `user_id_key_group` (`user_id`,`key`,`group`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

/**
Example User PARENT TABLE
*/
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('admin','user') NOT NULL DEFAULT 'user',
  `email` varchar(40) NOT NULL,
  `password` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `banned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `activated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

